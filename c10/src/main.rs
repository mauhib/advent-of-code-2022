use std::fs::File;
use std::io::prelude::*;
use std::io::BufReader;


enum Instruction {
    Noop,
    Addx(i32),
}

impl Instruction {
    fn cycles(&self) -> u32 {
        match self {
            Self::Noop => 1,
            Self::Addx(_) => 2,
        }
    }
}


fn program_from_file(filename: String) -> Vec<Instruction> {
    let f = File::open(filename).expect("Can't open file");
    let reader = BufReader::new(f);
    let instructions = reader.lines().map(|l| {
        let line = l.unwrap();
        let split = line.split(' ').collect::<Vec<&str>>();
        match split[0] {
            "noop" => Instruction::Noop,
            "addx" => Instruction::Addx(split[1].parse::<i32>().expect("Not a number")),
            _      => panic!("Illegal instruction"),
        }
    }).collect::<Vec<Instruction>>();
    return instructions;
}


fn generate_waveforms(init: i32, program: &Vec<Instruction>) -> Vec<i32> {
    let mut waveform: Vec<i32> = Vec::new();
    let mut reg = init;
    waveform.push(reg);
    for i in program.iter() {
        for _ in 0..i.cycles()-1 {
            waveform.push(reg);
        }
        match i {
            Instruction::Noop => (),
            Instruction::Addx(operand) => reg += operand,
        }
        waveform.push(reg);
    }
    return waveform;
}


fn get_signal_strength_sum(waves: &Vec<i32>) -> i32 {
    return (19..waves.len()).step_by(40).map(|i| waves[i] * ((i as i32)+1)).sum::<i32>();
}


fn draw_screen(waves: &Vec<i32>, x: u32, y: u32, sprite_len: u32) -> String {
    let mut s: String = String::new();
    for j in 0..y {
        for i in 0..x {
            let sprite_start = u32::try_from(waves[(j*x+i) as usize]-1);
            if sprite_start.is_ok() && i >= sprite_start.unwrap() && i < sprite_start.unwrap() + sprite_len {
                s.push('#');
            } else {
                s.push('.');
            }
        }
        s.push('\n');
    }
    return s;
}


fn main() {
    let program = program_from_file(String::from("input.txt"));
    let waves = generate_waveforms(1, &program);
    println!("Signal Strength Sum: {}", get_signal_strength_sum(&waves));

    println!("CRT Display:\n{}", draw_screen(&waves, 40, 6, 3))
}


#[test]
fn t1() {
    //let program = program_from_file(String::from("test.txt"));
    let program: Vec<Instruction> = vec![
        Instruction::Noop,
        Instruction::Addx(3),
        Instruction::Addx(-5),
    ];
    let waves = generate_waveforms(1, &program);
    assert_eq!(waves, [1, 1, 1, 4, 4, -1]);
}

#[test]
fn t2() {
    let program = program_from_file(String::from("test.txt"));
    let waves = generate_waveforms(1, &program);
    assert_eq!(waves[19],  21);
    assert_eq!(waves[59],  19);
    assert_eq!(waves[99],  18);
    assert_eq!(waves[139], 21);
    assert_eq!(waves[179], 16);
    assert_eq!(waves[219], 18);
    assert_eq!(get_signal_strength_sum(&waves), 13140);
    assert_eq!(draw_screen(&waves, 40, 6, 3),
    "##..##..##..##..##..##..##..##..##..##..\n\
     ###...###...###...###...###...###...###.\n\
     ####....####....####....####....####....\n\
     #####.....#####.....#####.....#####.....\n\
     ######......######......######......####\n\
     #######.......#######.......#######.....\n");
}
