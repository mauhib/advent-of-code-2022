use std::fs::File;
use std::io::prelude::*;
use std::io::BufReader;
use array_tool::vec::Uniq;


#[derive(Copy, Clone)]
enum Direction {
    Left,
    Right,
    Up,
    Down,
}

impl TryFrom<char> for Direction {
    type Error = &'static str;
    fn try_from(value: char) -> Result<Self, Self::Error> {
        match value {
            'L' => Ok(Direction::Left),
            'R' => Ok(Direction::Right),
            'U' => Ok(Direction::Up),
            'D' => Ok(Direction::Down),
            _   => Err("Conversion failed"),
        }
    }
}

struct Motion {
    direction: Direction,
    magnitude: u32,
}

#[derive(Clone, Copy, Debug)]
struct Point<N> {
    x: N,
    y: N,
}

impl PartialEq for Point<i32> {
    fn eq(&self, rhs: &Self) -> bool {
        self.x == rhs.x && self.y == rhs.y
    }
}

#[derive(Clone)]
struct Position {
    parts: Vec<Point<i32>>,
}

impl Position {
    fn new(parts: usize) -> Position {
        return Position {
            parts: vec![Point { x: 0, y: 0 }; parts],
        }
    }

    fn _to_grid_string(&self, x: i32, y: i32) -> String {
        let mut s = String::new();
        for j in (0..y).rev() {
            for i in 0..x {
                let mut c = '.';
                for (p, part) in self.parts.iter().enumerate().rev() {
                    if part.x == i && part.y == j {
                        c = (((p % 10) as u8) + ('0' as u8)) as char;
                    }
                }
                s.push(c);
            }
            s.push('\n');
        }
        return s;
    }
}


fn motions_from_file(filename: String) -> Vec<Motion> {
    let f = File::open(filename).expect("Can't open file");
    let reader = BufReader::new(f);
    let motions = reader.lines().map(|l| {
        let line = l.unwrap();
        let split = line.split(' ').collect::<Vec<&str>>();
        Motion {
            direction: Direction::try_from(split[0].chars().next().unwrap()).unwrap(),
            magnitude: split[1].parse::<u32>().expect("Expected a positive number"),
        }
    }).collect::<Vec<Motion>>();
    return motions;
}


fn follow_motions(position: Position, motions: &Vec<Motion>) -> Vec<Position> {
    let mut positions: Vec<Position> = Vec::new();
    let parts_len = position.parts.len();
    let mut p = position;
    for m in motions.iter() {
        for _ in 0..m.magnitude {
            //Move parts[0]
            match m.direction {
                Direction::Left  => p.parts[0].x -= 1 as i32,
                Direction::Right => p.parts[0].x += 1 as i32,
                Direction::Up    => p.parts[0].y += 1 as i32,
                Direction::Down  => p.parts[0].y -= 1 as i32,
            }

            for i in 1..parts_len {
                //If the parts[i-1] is ever two steps directly up, down, left, or right
                //from the parts[1], the parts[1] must also move one step in that direction
                if p.parts[i-1].x == p.parts[i].x {
                    //Move up
                    if p.parts[i-1].y - p.parts[i].y == 2 {
                        p.parts[i].y += 1;
                    }
                    //Move down
                    else if p.parts[i].y - p.parts[i-1].y == 2 {
                        p.parts[i].y -= 1;
                    }
                } else if p.parts[i-1].y == p.parts[i].y {
                    //Move right
                    if p.parts[i-1].x - p.parts[i].x == 2 {
                        p.parts[i].x += 1;
                    }
                    //Move left
                    else if p.parts[i].x - p.parts[i-1].x == 2 {
                        p.parts[i].x -= 1;
                    }
                }
                //if the parts[i-1] and parts[i] aren't touching and aren't in the same row or column,
                //the parts[i] always moves one step diagonally to keep up
                else if !((p.parts[i-1].x - p.parts[i].x).abs() == 1 && (p.parts[i-1].y - p.parts[i].y).abs() == 1) {
                    //Left / right
                    if p.parts[i-1].x > p.parts[i].x {
                        p.parts[i].x += 1;
                    } else {
                        p.parts[i].x -= 1;
                    }
                    //Up / Down
                    if p.parts[i-1].y > p.parts[i].y {
                        p.parts[i].y += 1;
                    } else {
                        p.parts[i].y -= 1;
                    }
                }
            }

            positions.push(p.clone());
        }
    }

    return positions;
}


fn total_tail_positions(positions: &Vec<Position>) -> u32 {
    let tail_positions = positions.iter().map(|x| x.parts.last().unwrap()).collect::<Vec<&Point<i32>>>();
    let unique_tail_positions = tail_positions.unique();
    return unique_tail_positions.len() as u32;
}

fn main() {
    let motions = motions_from_file(String::from("input.txt"));

    {
        let positions = follow_motions(Position::new(2), &motions);
        println!("Numbers of positions visited by tail (len: 2): {}", total_tail_positions(&positions));
    }

    {
        let positions = follow_motions(Position::new(10), &motions);
        println!("Numbers of positions visited by tail (len: 10): {}", total_tail_positions(&positions));
    }
}


#[test]
fn t1() {
    let motions = motions_from_file(String::from("test.txt"));
    assert_eq!(motions.len(), 8);
    let positions = follow_motions(Position::new(2), &motions);
    for (i, p) in positions.iter().enumerate() {
        println!("{}", i);
        println!("{}", p._to_grid_string(6, 5));
    }
    let final_position = positions.last().unwrap();
    assert_eq!(final_position.parts[0].x, 2);
    assert_eq!(final_position.parts[0].y, 2);
    assert_eq!(final_position.parts[1].x, 1);
    assert_eq!(final_position.parts[1].y, 2);
    assert_eq!(total_tail_positions(&positions), 13);
}

#[test]
fn t2() {
    let motions = motions_from_file(String::from("test2.txt"));
    assert_eq!(motions.len(), 8);
    let initial_position = Position {
        parts: vec![Point { x: 11, y: 5 }; 10]
    };
    let positions = follow_motions(initial_position, &motions);
    for (i, p) in positions.iter().enumerate() {
        println!("{}", i);
        println!("{}", p._to_grid_string(26, 21));
    }
    let final_position = positions.last().unwrap();
    for (i, p) in final_position.parts.iter().enumerate() {
        assert_eq!(p.x, 0);
        assert_eq!(p.y, 20 - (i as i32));
    }
    assert_eq!(total_tail_positions(&positions), 36);
}
