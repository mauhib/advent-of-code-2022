use std::fs::File;
use std::io::prelude::*;
use std::io::BufReader;

fn game_score(my_move: i32, op_move: i32) -> Result<i32, String> {
    match (my_move, op_move) {
        (1, 3) | (2, 1) | (3, 2)               => Ok(6),
        (i, j) if (i >= 1 && i <= 3 && i == j) => Ok(3),
        (3, 1) | (1, 2) | (2, 3)               => Ok(0),
        _ => Err("Illegal moves".to_string()),
    }
}

fn find_optimal_move(op_move: i32, outcome: i32) -> Result<i32, String> {
    match outcome {
        //Lose
        1 => match op_move {
            1 => Ok(3),
            2 => Ok(1),
            3 => Ok(2),
            _ => Err("Illegal move".to_string()),
        },
        //Draw
        2 => match op_move{
            1 | 2 | 3 => Ok(op_move),
            _ => Err("Illegal move".to_string()),
        },
        //Win
        3 => match op_move {
            1 => Ok(2),
            2 => Ok(3),
            3 => Ok(1),
            _ => Err("Illegal move".to_string()),
        },
        _ => Err("Illegal outcome".to_string()),
    }
}

fn main() {
    let filename = "input.txt";

    let f = File::open(filename).expect("Can't open file");
    let reader = BufReader::new(f);

    let rounds: Vec<Vec<u8>> = reader.lines().map(|l| {
        let line = l.unwrap();
        let tokens = line.split(" ").collect::<Vec<&str>>();
        assert!(tokens.len() == 2);
        return tokens.iter().map(|x| u8::try_from(x.chars().next().expect("Empty String found")).unwrap() ).collect::<Vec<u8>>();
    }).collect::<Vec<Vec<u8>>>();

    //Part 1
    let score1 = rounds.iter().map(|round| {
        let op_move = i32::from(round[0] - u8::try_from('A').unwrap() + 1);
        let my_move = i32::from(round[1] - u8::try_from('X').unwrap() + 1);
        return game_score(my_move, op_move).expect("Couldn't get game score") + my_move;
    }).sum::<i32>();
    println!("Score1: {}", score1);

    //Part 2
    let score2 = rounds.iter().map(|round| {
        let op_move = i32::from(round[0] - u8::try_from('A').unwrap() + 1);
        let outcome = i32::from(round[1] - u8::try_from('X').unwrap() + 1);
        let my_move: i32 = find_optimal_move(op_move, outcome).expect("Can't find optimal_move");
        return game_score(my_move, op_move).expect("Couldn't get game score") + my_move;
    }).sum::<i32>();
    println!("Score2: {}", score2);

}
