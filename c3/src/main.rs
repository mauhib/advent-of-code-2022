use std::fs::File;
use std::io::prelude::*;
use std::io::BufReader;
use array_tool::vec::Intersect;
use array_tool::vec::Uniq;


fn get_item_value(c: char) -> i32 {
    if c.is_ascii_lowercase() {
        i32::try_from(u8::try_from(c).unwrap() - u8::try_from('a').unwrap() + 1).unwrap()
    }
    else if c.is_ascii_uppercase() {
        i32::try_from(u8::try_from(c).unwrap() - u8::try_from('A').unwrap() + 27).unwrap()
    }
    else {
        panic!("Invalid item type")
    }
}


fn main() {
    let filename = "input.txt";

    let f = File::open(filename).expect("Can't open file");
    let reader = BufReader::new(f);

    let lines = reader.lines().map(|l| l.unwrap()).collect::<Vec<String>>();

    let total1: i32 = lines.iter().map(|line| {
        assert!(line.len() % 2 == 0);
        let (p1, p2) = line.split_at(line.len()/2);
        assert_eq!(line, &format!("{}{}", p1, p2));

        let p1_chars = p1.chars().collect::<Vec<char>>();
        let p2_chars = p2.chars().collect::<Vec<char>>();

        let common = p1_chars
            .intersect(p2_chars)
            .unique();

        return common.iter().map(|c| get_item_value(*c)).sum::<i32>();
    }).sum::<i32>();
    println!("Part 1: {:?}", total1);

    assert!(lines.len() % 3 == 0);
    let total2 = (0..lines.len()).step_by(3).map(|i| {
        let badges = lines[i].chars().collect::<Vec<char>>()
            .intersect(lines[i+1].chars().collect::<Vec<char>>())
            .intersect(lines[i+2].chars().collect::<Vec<char>>())
            .unique();
        assert_eq!(badges.len(), 1);
        return get_item_value(badges[0]);
    }).sum::<i32>();
    println!("Part 2: {:?}", total2);
}
