use std::fs::File;
use std::io::prelude::*;
use std::io::BufReader;
use array_tool::vec::Uniq;


fn get_distinct_index(buffer: &str, distinct_chars: usize) -> Option<usize> {
    let mut v = vec![' '; distinct_chars as usize];
    let mut ptr = 0;
    for (i, c) in buffer.chars().enumerate() {
        v[ptr] = c;
        if i >= distinct_chars-1 && v.is_unique() {
            return Some(i+1);
        }
        ptr = (ptr + 1) % distinct_chars;
    }
    return None;
}

fn process_packets(filename: &str, distinct_chars: usize) -> Vec<usize>{
    let f = File::open(filename).expect("Can't open file");
    let reader = BufReader::new(f);
    let lines = reader.lines().map(|l| l.unwrap()).collect::<Vec<String>>();

    let sop_indices = lines.iter().map(|l| get_distinct_index(l, distinct_chars).unwrap()).collect::<Vec<usize>>();
    return sop_indices;
}

fn main() {
    println!("Part 1: {:?}", process_packets("input.txt", 4));
    println!("Part 2: {:?}", process_packets("input.txt", 14));
}


#[test]
fn t1() {
    assert_eq!(process_packets("test.txt", 4), [7, 5, 6, 10, 11]);
}

#[test]
fn t2() {
    assert_eq!(process_packets("test.txt", 14), [19, 23, 23, 29, 26]);
}
