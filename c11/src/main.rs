extern crate pest;
#[macro_use]
extern crate pest_derive;
extern crate rug;

use std::collections::HashMap;
use std::cell::RefCell;
use std::fs;
use pest::Parser;
use pest::iterators::Pair;
use rug::{Integer, Complete};

#[derive(Parser)]
#[grammar = "monkey.pest"]
pub struct MonkeyParser;


struct Item {
    factors: HashMap<u32, u32>,
}


struct Monkey {
    items: RefCell<Vec<Integer>>,
    operands_str: Vec<String>,
    op: String,
    test_divisible: i32,
    pass_to_monkey: HashMap<bool, i32>,
}

impl Monkey {
    fn from_parsed(parsed: Pair<Rule>) -> Self {
        assert_eq!(parsed.as_rule(), Rule::monkey);
        let mut inner = parsed.into_inner();

        inner.next(); //ID

        let items = inner
                    .next().unwrap()
                    .into_inner()
                    .next().unwrap()
                    .into_inner()
                    .map(|x| x.as_str().parse::<Integer>().unwrap())
                    .collect::<Vec<Integer>>();

        let mut operation = inner.next().unwrap().into_inner();
        let mut operands_str = Vec::new();
        operands_str.push(operation.next().unwrap().as_str().to_string());
        let op = operation.next().unwrap().as_str().to_string();
        operands_str.push(operation.next().unwrap().as_str().to_string());

        let mut test = inner.next().unwrap().into_inner();
        let test_divisible = test.next().unwrap().as_str().trim().parse::<i32>().unwrap();

        let conditions = test.next().unwrap().into_inner();
        let mut pass_to_monkey = HashMap::new();
        for c in conditions {
            let mut condition = c.into_inner();
            pass_to_monkey.insert(
                condition.next().unwrap().as_str().parse::<bool>().unwrap(),
                condition.next().unwrap().as_str().trim().parse::<i32>().unwrap()
            );
        }

        Monkey {
            items: RefCell::new(items),
            operands_str,
            op,
            test_divisible,
            pass_to_monkey,
        }
    }

    fn do_operation(&self, old: Integer) -> Integer {
        assert_eq!(self.operands_str[0], "old");
        let operands = self.operands_str.iter().map(|o| match o.as_str() {
            "old" => old.clone(),
            _     => o.trim().parse::<Integer>().unwrap()
        }).collect::<Vec<Integer>>();
        match self.op.as_str(){
            "+" => operands.iter().sum::<Integer>(),
            "*" => (&operands[0] * &operands[1]).complete(),
            _   => panic!("Not supported"),
        }
    }
}


fn monkeys_from_file(filename: String) -> Vec<Monkey> {
    let unparsed = fs::read_to_string(filename).expect("Can't read file");
    let file = MonkeyParser::parse(Rule::file, &unparsed)
        .expect("Unsuccessful parse")
        .next().unwrap();

    let mut monkeys = Vec::new();
    for x in file.into_inner() {
        match x.as_rule() {
            Rule::monkey => {
                monkeys.push(Monkey::from_parsed(x));
            },
            Rule::EOI => (),
            _ => unreachable!(),
        }
    }
    return monkeys;
}


fn do_shenanigan_round(monkeys: &mut Vec<Monkey>, num_inspections: &mut Vec<i64>, worried: bool) {
    assert_eq!(monkeys.len(), num_inspections.len());
    for (m, monkey) in monkeys.iter().enumerate() {
        let items = monkey.items.borrow().clone();
        monkey.items.borrow_mut().clear();
        for i in items.iter() {
            let item = if !worried {
                monkey.do_operation(i.clone()) / 3
            } else if monkey.op == "*" && monkey.operands_str[0] == "old" && (monkey.operands_str[1] == "old" || i.clone().is_divisible_u(monkey.operands_str[1].trim().parse::<u32>().unwrap())) {
                i.clone()
            } else {
                monkey.do_operation(i.clone())
            };
            let divisible = item.is_divisible_u(monkey.test_divisible as u32);
            let new_monkey = monkey.pass_to_monkey.get(&divisible).unwrap();
            monkeys[*new_monkey as usize].items.borrow_mut().push(item);
            num_inspections[m] += 1;
        }
    }
}


fn calculate_monkey_business(num_inspections: &mut Vec<i64>) -> i64 {
    num_inspections.sort();
    let mut iter = num_inspections.iter().rev();
    return iter.next().unwrap() * iter.next().unwrap();
}


fn main() {
    //let mut monkeys = monkeys_from_file(String::from("input.txt"));
    //let mut num_inspections: Vec<i64> = vec![0; monkeys.len()];
    //for _round in 0..20 {
    //    do_shenanigan_round(&mut monkeys, &mut num_inspections, false);
    //}
    //let monkey_business = calculate_monkey_business(&mut num_inspections);
    //println!("Monkey Business {monkey_business}");

    let mut monkeys = monkeys_from_file(String::from("test.txt"));
    let mut num_inspections: Vec<i64> = vec![0; monkeys.len()];
    do_shenanigan_round(&mut monkeys, &mut num_inspections, true);
    for _round in 1..1100 {
        println!("Round: {}", _round);
        do_shenanigan_round(&mut monkeys, &mut num_inspections, true);
    }
    let monkey_business = calculate_monkey_business(&mut num_inspections);
    println!("Monkey Business: {monkey_business}");
}


#[test]
fn t1() {
    let mut monkeys = monkeys_from_file(String::from("test.txt"));
    //assert_eq!(*monkeys[0].items.borrow(), [79, 98]);
    //assert_eq!(*monkeys[1].items.borrow(), [54, 65, 75, 74]);
    //assert_eq!(*monkeys[2].items.borrow(), [79, 60, 97]);
    //assert_eq!(*monkeys[3].items.borrow(), [74]);

    let mut num_inspections = vec![0; monkeys.len()];

    do_shenanigan_round(&mut monkeys, &mut num_inspections, false);
    //assert_eq!(*monkeys[0].items.borrow(), [20, 23, 27, 26]);
    //assert_eq!(*monkeys[1].items.borrow(), [2080, 25, 167, 207, 401, 1046]);
    //assert_eq!(*monkeys[2].items.borrow(), []);
    //assert_eq!(*monkeys[3].items.borrow(), []);

    for _round in 1..20 {
        do_shenanigan_round(&mut monkeys, &mut num_inspections, false);
    }

    //assert_eq!(*monkeys[0].items.borrow(), [10, 12, 14, 26, 34]);
    //assert_eq!(*monkeys[1].items.borrow(), [245, 93, 53, 199, 115]);
    //assert_eq!(*monkeys[2].items.borrow(), []);
    //assert_eq!(*monkeys[3].items.borrow(), []);

    assert_eq!(num_inspections[0], 101);
    assert_eq!(num_inspections[1],  95);
    assert_eq!(num_inspections[2],   7);
    assert_eq!(num_inspections[3], 105);

    let monkey_business = calculate_monkey_business(&mut num_inspections);
    assert_eq!(monkey_business, 10605);
}


#[test]
fn test_prime() {
    use primes::is_prime;
    for f in ["test.txt", "input.txt"] {
        let monkeys = monkeys_from_file(String::from(f));
        for m in monkeys.iter() {
            assert!(is_prime(m.test_divisible as u64));
        }
    }
}


#[test]
fn t2() {
    let mut monkeys = monkeys_from_file(String::from("test.txt"));
    let mut num_inspections: Vec<i64> = vec![0; monkeys.len()];
    let mut rounds = 0;

    do_shenanigan_round(&mut monkeys, &mut num_inspections, true);
    rounds += 1;
    assert_eq!(num_inspections[0], 2);
    assert_eq!(num_inspections[1], 4);
    assert_eq!(num_inspections[2], 3);
    assert_eq!(num_inspections[3], 6);

    for _ in rounds..20 {
        do_shenanigan_round(&mut monkeys, &mut num_inspections, true);
        rounds += 1;
    }
    assert_eq!(num_inspections[0],  99);
    assert_eq!(num_inspections[1],  97);
    assert_eq!(num_inspections[2],   8);
    assert_eq!(num_inspections[3], 103);

    for _ in rounds..10_000 {
        do_shenanigan_round(&mut monkeys, &mut num_inspections, true);
        rounds += 1;
    }
    assert_eq!(num_inspections[0], 52166);
    assert_eq!(num_inspections[1], 47830);
    assert_eq!(num_inspections[2], 1938 );
    assert_eq!(num_inspections[3], 52013);

    let monkey_business = calculate_monkey_business(&mut num_inspections);
    assert_eq!(monkey_business, 2713310158);
}
