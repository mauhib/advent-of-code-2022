use std::fs::File;
use std::io::prelude::*;
use std::io::BufReader;
use std::rc::Rc;
use std::cell::RefCell;


struct Node {
    name: String,
    size: u32,
    parent: Option<Rc<Node>>,
    children: RefCell<Vec<Rc<Node>>>,
    is_file: bool,
}

impl Node {
    fn get_size(&self) -> u32 {
        return self.size + self.children.borrow().iter().map(|x| x.get_size()).sum::<u32>();
    }

    fn get_children(&self) -> Vec<Rc<Node>> {
        let mut v: Vec<Rc<Node>> = Vec::new();
        let children = self.children.borrow();
        for c in (*children).iter() {
            v.push(c.clone());
            v.extend(c.get_children());
        }
        return v;
    }
}

fn tree_from_file(filename: String) -> Rc<Node> {
    let f = File::open(filename).expect("Can't open file");
    let reader = BufReader::new(f);

    let tree_ptr: Rc<Node> = Rc::new(
        Node {
            name: String::from("/"),
            size: 0,
            children: RefCell::new(Vec::new()),
            parent: None,
            is_file: false,
        }
    );
    let mut cur_ptr: Rc<Node> = tree_ptr.clone();
    let mut list_requested = false;
    for l in reader.lines() {
        let line = l.unwrap();
        if line.starts_with("$ ") {
            list_requested = false;
            let split = line.split(" ").collect::<Vec<&str>>();
            match split[1] {
                "cd" => {
                    match split[2] {
                        "/" => {
                            cur_ptr = tree_ptr.clone();
                        },
                        ".." => {
                            let new_cur: &Rc<Node> = cur_ptr.parent.as_ref().expect("No parent to browse to");
                            cur_ptr = new_cur.clone();
                        },
                        name => {
                            let cur_ptr_borrowed = cur_ptr.clone();
                            let children = cur_ptr_borrowed.children.borrow();
                            let new_cur = children.iter().find(|&x| x.name == name).expect("No such directory found");
                            cur_ptr = new_cur.clone();
                        },
                    }
                },
                "ls" => {
                    list_requested = true;
                },
                _ => panic!("Command not Supported"),
            }
        } else if list_requested {
            let mut children_mut = cur_ptr.children.borrow_mut();
            let split = line.split(" ").collect::<Vec<&str>>();
            if split[0] == "dir" {
                children_mut.push(
                    Rc::new(Node {
                        name: String::from(split[1]),
                        size: 0,
                        children: RefCell::new(Vec::new()),
                        parent: Some(cur_ptr.clone()),
                        is_file: false,
                    })
                )
            } else {
                children_mut.push(
                    Rc::new(Node {
                        name: String::from(split[1]),
                        size: split[0].parse::<u32>().expect("Can't convert to unsigned number"),
                        children: RefCell::new(Vec::new()),
                        parent: Some(cur_ptr.clone()),
                        is_file: true,
                    })
                )
            }

        } else {
            panic!("Can't parse line");
        }
    }

    return tree_ptr;
}


fn get_total_size(tree: Rc<Node>) -> u32 {
    let mut nodes = tree.get_children();
    nodes.push(tree.clone());
    nodes = nodes.iter().map(|n| n.clone()).filter(|n| !n.is_file && n.get_size() <= 100_000).collect::<Vec<Rc<Node>>>();
    return nodes.iter().map(|n| n.get_size()).sum::<u32>();
}


fn get_dir_size_to_delete(tree: Rc<Node>) -> u32 {
    let total_space: u32 = 70_000_000;
    let total_space_needed: u32 = 30_000_000;
    let used_space = &tree.get_size();
    assert!(total_space_needed + used_space > total_space);
    let space_needed = total_space_needed + used_space - total_space;

    let mut nodes = tree.get_children();
    nodes.push(tree.clone());
    let nodes = nodes.iter().filter(|n| !n.is_file && n.get_size() >= space_needed).collect::<Vec<&Rc<Node>>>();

    let n: &Rc<Node> = nodes.iter().min_by(|&x,&y| x.get_size().cmp(&y.get_size())).expect("No directories found");

    return n.get_size();
}


fn main() {
    let tree: Rc<Node> = tree_from_file(String::from("input.txt"));
    println!("Total Size: {}", get_total_size(tree.clone()));
    println!("Dir size to delete: {}", get_dir_size_to_delete(tree.clone()));
}


#[test]
fn t1() {
    let tree: Rc<Node> = tree_from_file(String::from("test.txt"));
    assert_eq!(get_total_size(tree), 95437);
}

#[test]
fn t2() {
    let tree: Rc<Node> = tree_from_file(String::from("test.txt"));
    assert_eq!(get_dir_size_to_delete(tree), 24_933_642);
}
