use std::fs::File;
use std::io::Read;
use regex::Regex;

fn main() {
    let filename = "input.txt";

    let mut f = File::open(filename).expect("Can't open file");
    let mut content = String::new();
    f.read_to_string(&mut content).expect("Can't read file");

    let mut elf_calories = Regex::new(r"\r*\n\r*\n").unwrap().split(&content).map(|elf| {
        Regex::new(r"\r*\n").unwrap().split(&elf).map(|calories| {
            match calories {
                "" => 0,
                _ => calories.parse::<i32>().expect(&format!("Value {} is not an int", calories))
            }
        }).sum::<i32>()
    }).collect::<Vec<i32>>();

    println!("Max calories carried by any elf: {}", elf_calories.iter().max().unwrap());

    elf_calories.sort();
    elf_calories.reverse();
    println!("Total calories carried by top 3 elves: {:?}", elf_calories[0..3].iter().sum::<i32>());
}
