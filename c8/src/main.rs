use std::fs::File;
use std::io::prelude::*;
use std::io::BufReader;


fn grid_from_file(filename: String) -> Vec<Vec<i32>>{
    let f = File::open(filename).expect("Can't open file");
    let reader = BufReader::new(f);
    let grid = reader.lines().map(|l| l.unwrap().chars().map(|c| (c as i32)-('0' as i32)).collect::<Vec<i32>>()).collect::<Vec<Vec<i32>>>();
    return grid;
}

fn find_visible_trees(g: &Vec<Vec<i32>>) -> Vec<Vec<bool>> {
    let x = g.first().unwrap().len();
    let y = g.len();
    let mut visible = vec![vec![false; x]; y];

    //From left
    for j in 0..y {
        let mut max = -1;
        for i in 0..x {
            if g[j][i] >  max {
                visible[j][i] = true;
                max = g[j][i]
            }
        }
    }

    //From right
    for j in 0..y {
        let mut max = -1;
        for i in (0..x).rev() {
            if g[j][i] >  max {
                visible[j][i] = true;
                max = g[j][i]
            }
        }
    }

    //From Top
    for i in 0..x {
        let mut max = -1;
        for j in 0..y {
            if g[j][i] >  max {
                visible[j][i] = true;
                max = g[j][i]
            }
        }
    }

    //From Bottom
    for i in 0..x {
        let mut max = -1;
        for j in (0..y).rev() {
            if g[j][i] >  max {
                visible[j][i] = true;
                max = g[j][i]
            }
        }
    }

    return visible;
}


fn find_scenic_scores(g: &Vec<Vec<i32>>) -> Vec<Vec<i32>> {
    let x = g.first().unwrap().len();
    let y = g.len();
    let mut score = vec![vec![0; x]; y];

    for j in 0..y {
        for i in 0..x {
            //Look Left
            let mut vd_l = 0;
            for a in (0..i).rev() {
                vd_l += 1;
                if g[j][a] >= g[j][i] {
                    break;
                }
            }
            //Look Right
            let mut vd_r = 0;
            for a in (i+1)..x {
                vd_r += 1;
                if g[j][a] >= g[j][i] {
                    break;
                }
            }
            //Look Up
            let mut vd_u = 0;
            for b in (0..j).rev() {
                vd_u += 1;
                if g[b][i] >= g[j][i] {
                    break;
                }
            }
            //Look Down
            let mut vd_d = 0;
            for b in (j+1)..y {
                vd_d += 1;
                if g[b][i] >= g[j][i] {
                    break;
                }
            }
            score[j][i] = vd_l * vd_r * vd_u * vd_d;
        }
    }

    return score;
}


fn total_visible(visible: &Vec<Vec<bool>>) -> i32 {
    return visible.iter().map(|y| y.iter().map(|&x| x as i32).sum::<i32>()).sum::<i32>();
}


fn max_2d(v: &Vec<Vec<i32>>) -> i32 {
    let max: i32 = *v.iter().map(|y| y.iter().max().unwrap()).max().unwrap();
    return max;
}


fn main() {
    let trees = grid_from_file(String::from("input.txt"));
    let visible = find_visible_trees(&trees);
    println!("Total Visible: {}", total_visible(&visible));
    let ss = find_scenic_scores(&trees);
    println!("Max Scenic Score: {}", max_2d(&ss));
}


#[test]
fn t1() {
    let trees = grid_from_file(String::from("test.txt"));
    let visible = find_visible_trees(&trees);
    assert_eq!(visible, [
        [true , true , true , true , true ],
        [true , true , true , false, true ],
        [true , true , false, true , true ],
        [true , false, true , false, true ],
        [true , true , true , true , true ],
    ]);
    assert_eq!(total_visible(&visible), 21);
}

#[test]
fn t2() {
    let trees = grid_from_file(String::from("test.txt"));
    let ss = find_scenic_scores(&trees);
    assert_eq!(ss[1][2], 4);
    assert_eq!(ss[3][2], 8);
    assert_eq!(max_2d(&ss), 8);
}
