use std::fs::File;
use std::io::prelude::*;
use std::io::BufReader;
use std::collections::VecDeque;
use regex::Regex;

fn run_instructions_on_stacks(stacks: &mut Vec<VecDeque<char>>, instructions: &[String], keep_order: bool) {
    let move_re = Regex::new(r"move (\d+) from (\d+) to (\d+)").unwrap();
    for instruction in instructions.iter().enumerate().filter(|&(k,_)| k != 0).map(|(_,v)| v) {
        let captures = move_re.captures(instruction).expect(&format!("Regex doesn't match on {}", instruction));
        let num  = captures[1].parse::<i32>().unwrap();
        let from = captures[2].parse::<usize>().unwrap() - 1;
        let to   = captures[3].parse::<usize>().unwrap() - 1;
        if !keep_order {
            for _ in 0..num {
                let popped = stacks[from].pop_front().expect("Pop from empty stack");
                stacks[to].push_front(popped);
            }
        }
        else {
            let mut queue = VecDeque::<char>::new();
            for _ in 0..num {
                let popped = stacks[from].pop_front().expect("Pop from empty stack");
                queue.push_front(popped);
            }
            for _ in 0..num {
                let popped = queue.pop_front().expect("Pop from empty stack");
                stacks[to].push_front(popped);
            }
        }
    }
}

fn stacks_tops(stacks: &Vec<VecDeque<char>>) -> String {
    return stacks.iter().map(|s| *(s.front().expect("Empty Stack"))).collect::<String>();
}

fn main() {
    let filename = "input.txt";

    let f = File::open(filename).expect("Can't open file");
    let reader = BufReader::new(f);
    let lines = reader.lines().map(|l| l.unwrap()).collect::<Vec<String>>();

    let (stacks_raw, instructions) = lines.split_at(lines.iter().position(|x| x == "").expect("Can't find seperation between stacks and instructions"));

    let stack_indices = stacks_raw
        .last()
        .unwrap()
        .chars()
        .enumerate()
        .filter(|&(_,v)| v != ' ')
        .map(|(k,_)| k)
        .collect::<Vec<usize>>();
    let mut stacks: Vec<VecDeque<char>> = Vec::new();
    for _ in 0..stack_indices.len() {
        stacks.push(VecDeque::new());
    }
    for line in stacks_raw.iter().rev().enumerate().filter(|&(k,_)| k != 0).map(|(_,v)| v) {
        let line_bytes = line.as_bytes();
        for (stack_index, char_index) in stack_indices.iter().enumerate().filter(|&(_,&v)| v < line_bytes.len() && line_bytes[v] as char != ' ') {
            stacks[stack_index].push_front(line_bytes[*char_index] as char);
        }
    }

    let mut stacks1 = stacks.clone();
    run_instructions_on_stacks(&mut stacks1, instructions, false);
    println!("Part 1: {}", stacks_tops(&stacks1));

    let mut stacks2 = stacks.clone();
    run_instructions_on_stacks(&mut stacks2, instructions, true);
    println!("Part 2: {}", stacks_tops(&stacks2));
}
