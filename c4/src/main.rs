use std::fs::File;
use std::io::prelude::*;
use std::io::BufReader;

fn is_a_in_b(a_start: i32, a_end: i32, b_start: i32, b_end: i32) -> bool {
    return b_start <= a_start && b_end >= a_end;
}

fn find_overlap(a_start: i32, a_end: i32, b_start: i32, b_end: i32) -> bool {
    return (b_start <= a_start && a_start <= b_end) || (b_start <= a_end && a_end <= b_end) ||
           (a_start <= b_start && b_start <= a_end) || (a_start <= b_end && b_end <= a_end);
}

fn main() {
    let filename = "test.txt";

    let f = File::open(filename).expect("Can't open file");
    let reader = BufReader::new(f);

    let lines = reader.lines().map(|l| l.unwrap()).collect::<Vec<String>>();

    let contained = lines.iter().filter(|&line| {
        let p = line.split(",").map(|x| x.split("-").map(|y| y.parse::<i32>().unwrap()).collect::<Vec<i32>>()).collect::<Vec<Vec<i32>>>();
        return is_a_in_b(p[0][0], p[0][1], p[1][0], p[1][1]) || is_a_in_b(p[1][0], p[1][1], p[0][0], p[0][1]);
    }).count();
    println!("{}", contained);

    let overlapping = lines.iter().filter(|&line| {
        let p = line.split(",").map(|x| x.split("-").map(|y| y.parse::<i32>().unwrap()).collect::<Vec<i32>>()).collect::<Vec<Vec<i32>>>();
        return find_overlap(p[0][0], p[0][1], p[1][0], p[1][1]) || is_a_in_b(p[1][0], p[1][1], p[0][0], p[0][1]);
    }).count();
    println!("{}", overlapping);
}
